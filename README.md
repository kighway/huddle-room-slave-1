# README

i am written in pure python -- no flask, no django

all of my libraries come by default on the latest NOOBS version for Raspberry Pi

connect the data cable of the motion sensor to pin 4, close the power circuit, and i will work

this is a single file app obviously

i send patch requests to the url

if no motion has been detected in the media room (or whatever room you put me in), i change the open_status of the room

open_status = true // means the room is vacant/ unoccupied/ available
