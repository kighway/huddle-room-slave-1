from gpiozero import MotionSensor
import requests
import time
from time import gmtime, strftime
import json

url = "https://nameless-beyond-68958.herokuapp.com/media"

update_frequency = 300 #in seconds; 300s = 5min

previous_detection = time.time()

def send_patch_request( open_status ):
    "sends patch requests and prints debuggig info to terminal "
    patch_request = requests.patch(url, { 'open_status' : str(open_status).lower() } )
    status_code = str(patch_request.status_code)
    room_state = "vacant" if open_status else "occupied"
    gmt_time = strftime("%Y-%m-%d %H:%M:%S", gmtime()) #for debugging
    print "status code " + status_code + ", room " + room_state + " on " + gmt_time + " GMT"

open_status = False
send_patch_request( open_status )

pir = MotionSensor(4)

while True:
    current_detection = time.time()
    time_since_last_detection = current_detection - previous_detection 
    if  time_since_last_detection > update_frequency and open_status == False:
        open_status = True
        send_patch_request( open_status )
    if pir.motion_detected:
        current_detection = time.time()
        if time_since_last_detection > update_frequency and open_status == True:
            open_status = False
            send_patch_request( open_status )
            previous_detection = current_detection
        

    
